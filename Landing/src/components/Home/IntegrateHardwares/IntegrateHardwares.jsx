import React from "react";
import { Menu, Icon ,Grid , Select, Button } from 'antd';
import './IntegrateHardwares.css';
import { Row, Col} from 'antd';
class IntegrateHardwares extends React.Component {
render() {
return (
/* start section Integrate Hardwares */
<div className="intergate">
    <div className="intergate-title">
        <h3> WE INTEGRATE WITH HARDWARES</h3>
    </div>
    <div className="container">
        <Row gutter={24}>
            <div className="intergate-holder">
                <Row>
                    <Col md={24}>
                    <Col xs={12} sm={6} md={6} lg={6} xl={6}>
                    <div className="intergate-icon">
                        <img src={require('../../../img/tablet.png')}/>
                        <h3> Tablet </h3>
                    </div>
                    </Col>
                     <Col xs={12} sm={6} md={6} lg={6} xl={6}>

                    <div className="intergate-icon">
                        <img src={require('../../../img/scanner.png')}/>
                        <h3> Tablet </h3>
                    </div>
                    </Col>
                   <Col xs={12} sm={6} md={6} lg={6} xl={6}>

                    <div className="intergate-icon">
                        <img src={require('../../../img/print.png')}/>
                        <h3> Tablet </h3>
                    </div>
                    </Col>
                    <Col xs={12} sm={6} md={6} lg={6} xl={6}>

                    <div className="intergate-icon">
                        <img src={require('../../../img/cards.png')}/>
                        <h3> Tablet </h3>
                    </div>
                    </Col>
                    </Col>
                </Row>
            </div>
        </Row>
    </div>
</div>
/* end  section Integrate Hardwares */
);
}
}
export default IntegrateHardwares;