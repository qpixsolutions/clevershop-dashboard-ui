import React from "react";
import { Menu, Icon ,Grid , Select, Button } from 'antd';
import './Header.css';
import { Row, Col} from 'antd';
const { SubMenu } = Menu;
const { Option } = Select;
function handleChange(value) {
console.log(`selected ${value}`);
}
class Header extends React.Component {
state = {
current: 'mail',
};
handleClick = e => {
console.log('click ', e);
this.setState({
current: e.key,
});
};
render() {
return (
/* Start Section Header */
<div className="header">
    <div className="container">
        <Row>
            <Col md={4}>
            <div className="header-logo">
                <img src={require('../../../img/home-logo.svg')}/> 
            </div>
            </Col> 
            <Col md={20}>
            <div className="header-nav">
                <Menu onClick={this.handleClick} selectedKeys={[this.state.current]} mode="horizontal">
                    <Menu.Item key="mail">
                        About
                    </Menu.Item>
                    <Menu.Item key="mail">
                        Blog
                    </Menu.Item>
                    <Menu.Item key="mail">
                        Community
                    </Menu.Item>
                    <Menu.Item key="mail">
                        FAQ
                    </Menu.Item>
                    <Menu.Item key="mail">
                        White Paper
                    </Menu.Item>
                </Menu>
            </div>
            </Col>
        </Row>
        <div className="header-caption">
            <h3>What is <br />
                Cleversell?
            </h3>
            <p> qpix developed a brand new POS software scaling <br />
                the use from such selling/buying and making.
            </p>
        </div>
    </div>
</div>
/* End Section Header */
);
}
}
export default Header;