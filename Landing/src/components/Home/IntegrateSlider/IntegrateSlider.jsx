import React from "react";
import { Menu, Icon ,Grid , Select, Button } from 'antd';
import './IntegrateSlider.css';
import { Row, Col} from 'antd';
import Slider from "react-slick";
class IntegrateSlider extends React.Component {
render() {
    var settings = {
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        initialSlide: 0,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: false
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2,
              infinite: true,

            }
          },
          {
            breakpoint: 414,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      };
return (
/* start section Integrate Slider */
<div className="intergate-clintes">
    <div className="intergate-title">
        <h3> WE INTEGRATE WITH HARDWARES</h3>
    </div>
    <div className="container">
        <Row gutter={16}>
            <div className="container">
                <Slider {...settings}>
                    <div   className="intergate-clintes_holder gutter-row">
                        <blockquote>text of the printing and typesetting 
                            industry. Lorem Ipsum has been the industry's
                            standard dummy text ever since the 1500s,
                        </blockquote>
                        <div className="intergate-clintes_info">
                            <img src={require('../../../img/zara.jpg')}/>
                            <div className="intergate-clintes_details">
                                <p>Zara</p>
                                <h3> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </h3>
                            </div>
                        </div>
                    </div>
                    <div   className="intergate-clintes_holder gutter-row">
                        <blockquote> text of the printing and typesetting 
                            industry. Lorem Ipsum has been the industry's
                            standard dummy text ever since the 1500s,
                        </blockquote>
                        <div className="intergate-clintes_info">
                            <img src={require('../../../img/zara.jpg')}/>
                            <div className="intergate-clintes_details">
                                <p>Zara</p>
                                <h3> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </h3>
                            </div>
                        </div>
                    </div>
                    <div   className="intergate-clintes_holder gutter-row">
                        <blockquote> text of the printing and typesetting 
                            industry. Lorem Ipsum has been the industry's
                            standard dummy text ever since the 1500s,
                        </blockquote>
                        <div className="intergate-clintes_info">
                            <img src={require('../../../img/zara.jpg')}/>
                            <div className="intergate-clintes_details">
                                <p>Zara</p>
                                <h3> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </h3>
                            </div>
                        </div>
                    </div>
                    <div  className="intergate-clintes_holder gutter-row">
                        <blockquote> text of the printing and typesetting 
                            industry. Lorem Ipsum has been the industry's
                            standard dummy text ever since the 1500s,
                        </blockquote>
                        <div className="intergate-clintes_info">
                            <img src={require('../../../img/zara.jpg')}/>
                            <div className="intergate-clintes_details">
                                <p>Zara</p>
                                <h3> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </h3>
                            </div>
                        </div>
                    </div>
                </Slider>
            </div>
        </Row>
    </div>
</div>
/* End section Integrate Slider */
);
}
}
export default IntegrateSlider;