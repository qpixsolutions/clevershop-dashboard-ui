import React from "react";
import { Menu, Icon ,Grid , Select, Button } from 'antd';
import './RetailInterface.css';
import { Row, Col} from 'antd';
class RetailInterface extends React.Component {
render() {
return (
/* start section  Retailer interface */
<div className="retail-interface">
    <div className="container">
    <Col xs={24} sm={24} md={24} lg={8} xl={8}>
        <h1> Retail Cleversell  <br/> interface</h1>
        <p> when an unknown printer took a galley of type and scrambled it to make a
            type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining.
        </p>
        <div className="retail-interface__button">
            <Button type="primary">Join</Button>
        </div>
        </Col>
        <Col xs={24} sm={24} md={24} lg={16} xl={16}>
        <img src={require('../../../img/retails.jpg')}/>
        </Col>
    </div>
</div>
/* End section  Retailer interface */
);
}
}
export default RetailInterface;