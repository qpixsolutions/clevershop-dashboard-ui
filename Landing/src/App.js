import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Rate } from 'antd';
import 'antd/dist/antd.css'; // orr 'antd/dist/antd.less'
import Home from './components/Home/Home';

function App() {
  return (
    <div className="App">
    <Home/>
    </div>
  );
}

export default App;
