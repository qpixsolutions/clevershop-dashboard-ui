
import React, { Component } from "react";
import { Grid ,Row } from "react-bootstrap";
import './NavbarAdd.css'
import { Input  ,Button, Icon} from 'antd';
const { Search } = Input;

class NavbarAdd extends Component {
  render() {
    return (

      <div className="NavbarAdd">
            <div className="NavbarAdd-bg">
                


<Row> 


     <div className="col-md-8">
   
     <Search
      placeholder="Search"
      onSearch={value => console.log(value)}
    
    />
    

      </div>
      <div className="col-md-4">


<Button className="NavbarAdd_button" type="primary">
<Icon type="plus" />

            New Something
          </Button>
    </div>
    </Row>
    </div>
    
    </div>

    );
  }
}

export default NavbarAdd;
