
import React, { Component } from "react";
import { Grid ,Row } from "react-bootstrap";
import './CustomerForm.css'
import { Input  ,Button, Icon ,Table, Divider } from 'antd';
const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      
      render: text => <a>{text}</a>,
      
    },
    {
      title: 'Phone number',
      dataIndex: 'phonenumber',
      key: 'phonenumber',
    },
   
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
      },
      {
        title: 'Branch',
        dataIndex: 'branch',
        key: 'branch',
      },
 
    {
      title: 'Edit',
      key: 'action',
      render: (text, record) => (
        <span>
          <a href="#"><Icon type="edit" /> </a>
                  </span>
      ),
    },
  ];
  
  const data = [
    {
      key: '1',
      name: 'Mariam ahmed',
      phonenumber: '01211115070',
      email:'Mail@hotmail.com',
      branch: 'Point 90',

    },
    {
      key: '2',
      name: 'Ahmed Essa',
      phonenumber: 42,
      email:'Mail@hotmail.com',
      branch: 'Point 90',

    },
    {
      key: '3',
      name: 'Hatem Mamoun',
      phonenumber: 32,
      email:'Mail@hotmail.com',
      branch: 'Point 90',

    },
  ];
class CustomerForm extends Component {
    
  render() {
      
    return (

        
      <div className="CustomerForm">
             
<Table  responsive columns={columns} dataSource={data} />
    
    </div>

    );
  }
}

export default CustomerForm;
